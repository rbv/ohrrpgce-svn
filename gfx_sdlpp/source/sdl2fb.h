//sdl2fb.h
//(C) Copyright 2010 Jay Tennant and the OHRRPGCE Developers
//Dual licensed under the GNU GPL v2+ and MIT Licenses. Read LICENSE.txt for terms and disclaimer of liability.
//
//converts sdl keysym's to fb scancodes
//only concerned with certain keys, though

#ifndef GFX_SDL2FB_H
#define GFX_SDL2FB_H

int sdl2fb(int sdlCode);

#endif
